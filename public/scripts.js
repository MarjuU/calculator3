$(document).ready(function () {
    yearlyCalculation();

    //cannot enter manually the floor count:
    $('.count').prop('disabled', true);

    // to make the +/- buttons work:
    $('.plus-button').on('click', function (event) {
        var siblingTarget = event.currentTarget.previousElementSibling.firstElementChild
        var valueSibling = parseInt(siblingTarget.value)
        siblingTarget.value = valueSibling + 1
        var parent = $(siblingTarget).parents().get(2)
        $(parent).addClass("room-selected")
        var child = parent.firstElementChild.lastElementChild
        $(child).addClass("room-W-m2-selected")
        yearlyCalculation()
    } );

    $('.minus-button').on('click', function (event) {
        var siblingTarget = event.currentTarget.nextElementSibling.lastElementChild
        var valueSibling = parseInt(siblingTarget.value)
        if (valueSibling > 0)   {
            valueSibling = valueSibling - 1
            siblingTarget.value = valueSibling
        }

        if (valueSibling == 0) {
            var parent = $(siblingTarget).parents().get(2)
            $(parent).removeClass("room-selected")
            var child = parent.firstElementChild.lastElementChild
            $(child).removeClass("room-W-m2-selected")
        }
        yearlyCalculation()
    });

    //recalculate cost and savings when floorspace altered manually
    $('#floorspace-range').on('input', function()   {
        $("#floorspace-no").val($('#floorspace-range').val())
    });

    $('#floorspace-range').change( function()   {
        $("#floorspace-no").val($('#floorspace-range').val())
        yearlyCalculationFloorSpaceSame()
    });

    $('#floorspace-no').change(function()   {
        $("#floorspace-range").val($('#floorspace-no').val())
        yearlyCalculationFloorSpaceSame()
    });

    $('.fixed-price').change(function(event) {
        if($(".fixed-price").val() == 0)   {
            $(".fixed-price").val(1)
        } else {
            if($(".fixed-price").val() == 1)   {
                $(".fixed-price").val(0)
            }
        }
        yearlyCalculationFloorSpaceSame()
    });
});

//calculate yearly costs and savings and base it on floorspace calculation based on entered rooms
function yearlyCalculation() {
    var bathroomCount = parseFloat($("#bathroom").val())
    var livingSpaceCount = parseFloat($("#living-spaces").val())
    var kitchenCount = parseFloat($("#kitchen").val())
    var hallwayCount = parseFloat($("#hallway").val())


    floorSpaceCalculation(bathroomCount, livingSpaceCount, kitchenCount, hallwayCount)

    endCalculations(bathroomCount, livingSpaceCount, kitchenCount, hallwayCount)

}

//calculate yearly costs and savings when floor space calculation based on rooms is not needed (range or total number changed)
function yearlyCalculationFloorSpaceSame()   {
    var bathroomCount = parseFloat($("#bathroom").val())
    var livingSpaceCount = parseFloat($("#living-spaces").val())
    var kitchenCount = parseFloat($("#kitchen").val())
    var hallwayCount = parseFloat($("#hallway").val())

    endCalculations(bathroomCount, livingSpaceCount, kitchenCount, hallwayCount)

}

//calculate the floorspace based on entered rooms
function floorSpaceCalculation(bathroomCount, livingSpaceCount, kitchenCount, hallwayCount)  {
    $(".floorspace-calc").val(
        (bathroomCount * 8) + (livingSpaceCount * 15) + (kitchenCount * 6) + (hallwayCount * 5))

}

//calculate yearly costs and savings
function endCalculations(bathroomCount, livingSpaceCount, kitchenCount, hallwayCount)   {

    var roomCount = bathroomCount + livingSpaceCount + kitchenCount + hallwayCount

    $('#themos-output').val(roomCount)

    if(roomCount == 0) {
        roomCount = 1
        $('#themos-output').val(0)
        $('#payback-output').val(0)
        $(".min-values").hide()
    }

    var wperM2Max = ((bathroomCount * 160) + (livingSpaceCount * 80) + (kitchenCount * 100) + (hallwayCount * 160)) / roomCount
    var wperM2Min = ((bathroomCount * 55) + (livingSpaceCount * 60) + (kitchenCount * 55) + (hallwayCount * 55)) / roomCount

    var thirtyPercentMax = wperM2Max * 0.3
    var thirtyPercentMin = wperM2Min * 0.3

    var floorSpaceValue = parseFloat($("#floorspace-no").val())
    console.log(floorSpaceValue)

    if(isNaN(floorSpaceValue)) {
        $("#floorspace-no").val(3)
        floorSpaceValue = 3
        $("#floorspace-range").val(3)

    }

    var yearlyMaxConsumption = ((thirtyPercentMax * floorSpaceValue * 4220) / 1000).toFixed(0)
    var yearlyMinConsumption = ((thirtyPercentMin * floorSpaceValue * 4220) / 1000).toFixed(0)

    $("#consumption-output-max").val(yearlyMaxConsumption)

    if (yearlyMinConsumption > 0)   {
        $(".min-values").show()
    }

    $("#consumption-output-min").val(yearlyMinConsumption)
    var priceTagMax = (yearlyMaxConsumption * 0.17).toFixed(0)
    var priceTagMin = (yearlyMinConsumption * 0.17).toFixed(0)

    $('#year-price-output-max').val(priceTagMax)
    $('#year-price-output-min').val(priceTagMin)

    var savingsPercentage


    if ($('.fixed-price').val() == 1) {
        savingsPercentage = 0.2
    }

    else {
        savingsPercentage = 0.325

    }

    var savingsPerYearMin = (priceTagMin * savingsPercentage).toFixed(0)
    var savingsPerYearMax = (priceTagMax * savingsPercentage).toFixed(0)

    $('#savings-output-min').val(savingsPerYearMin)
    $('#savings-output-max').val(savingsPerYearMax)

    var carbonSavedMin = (((yearlyMinConsumption * 500) * 0.325) / 1000).toFixed(0)
    var carbonSavedMax = (((yearlyMaxConsumption * 500) * 0.325) / 1000).toFixed(0)

    $('#carbon-output-min').val(carbonSavedMin)
    $('#carbon-output-max').val(carbonSavedMax)

    var payBackPeriod = 0

    if (savingsPerYearMax == 0) {
        payBackPeriod = 0
    }
    else {
        payBackPeriod = ((99 * roomCount) / savingsPerYearMax).toFixed(1)
    }

    $('#payback-output').val(payBackPeriod)
}